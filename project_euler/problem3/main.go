package main

import "fmt"

func main() {
	mainNumber := 600851475143
	startCounter := 600851475143 / 2
	var resultPrime int

	for i := 2; i <= startCounter; i++ {

		if mainNumber%i == 0 {
			resultPrime = i
		}
	}
	fmt.Println(resultPrime)
}
