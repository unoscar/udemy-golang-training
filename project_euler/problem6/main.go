// This file will return the result of the substraction
// of  the sum of the squares of the first one hundred
// natural number minus the square of the sum
package main

import "fmt"

func main() {
	var firstSum int
	var secondSum int

	for n := 1; n <= 100; n++ {
		firstSum += n * n
		secondSum += n
	}
	secondSum = secondSum * secondSum
	result := secondSum - firstSum
	fmt.Println(result)
}
