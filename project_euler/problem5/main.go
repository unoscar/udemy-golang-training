package main

import "fmt"

func main() {
	startNumber := 1

	for 1 == 1 {

		if isDivisible(startNumber) {
			break
		}
		startNumber++
	}
	fmt.Println(startNumber)
}

func isDivisible(n int) bool {
	r := true

	for i := 1; i <= 20; i++ {

		if n%i != 0 {
			r = false
		}
	}
	return r
}
