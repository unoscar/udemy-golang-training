package main

import "fmt"

func main() {
	var counter int
	var oldRecord int
	newRecord := 1

	for newRecord < 4000000 {
		var sum int

		if newRecord%2 == 0 {
			counter += newRecord
		}
		sum = newRecord + oldRecord
		oldRecord = newRecord
		newRecord = sum

	}
	fmt.Println(counter)
}
